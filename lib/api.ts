export const fetcher = async ({ url, method, body, json = true }) => {
  const res = await fetch(url, {
    method,
    //body: body && JSON.stringify(body),
    ...(body && { body: JSON.stringify(body) }), // If body is true, spread body object
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  if (!res.ok) {
    throw new Error("API error");
  }

  if (json) {
    const data = await res.json();
    return data.data;
  }
};

interface RegisterUser {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}

export const register = (user: RegisterUser) => {
  return fetcher({ url: "/api/register", method: "post", body: user });
};

export interface SignInUser {
  email: string;
  password: string;
}

export const signin = (user: SignInUser) => {
  return fetcher({ url: "/api/signin", method: "post", body: user });
};
